import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {

  inicio:number = 0;

  funcionb1Off(){console.log("el boton 1 no se presiono"
  );}
  funcionb1On(){
    this.inicio = 1;
  };

  funcionb2Off(){
    console.log("el boton 2 no se presiono");
  }
  funcionb2On(){
    console.log("el boton 2 se preciono");
    this.inicio=2;};

  funcionb3Off(){
    console.log("el boton 3 no se presiono");
  }
  funcionb3On(){
    console.log("el boton 3 se preciono");
    this.inicio=3;
  }

  constructor(

  ) {
    
    this.funcionb1Off();
    this.funcionb2Off();
    this.funcionb3Off();
  }

  ngOnInit(): void {
  }
}
